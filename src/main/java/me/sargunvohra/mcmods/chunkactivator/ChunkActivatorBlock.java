package me.sargunvohra.mcmods.chunkactivator;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

import java.util.Objects;

public class ChunkActivatorBlock extends BlockWithEntity {
    public ChunkActivatorBlock() {
        super(
            FabricBlockSettings.of(Material.METAL, MaterialColor.GOLD)
                .strength(3f, 6f)
                .sounds(BlockSoundGroup.METAL)
        );
    }

    @Override
    public BlockEntity createBlockEntity(BlockView blockView) {
        return new ChunkActivatorBlockEntity();
    }

    @Override
    public BlockRenderType getRenderType(BlockState blockState_1) {
        return BlockRenderType.MODEL;
    }

    @Override
    public void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean notify) {
        ((ChunkActivatorBlockEntity) Objects.requireNonNull(world.getBlockEntity(pos))).onPlaced();
    }
}
