package me.sargunvohra.mcmods.chunkactivator;

import nerdhub.cardinal.components.api.ComponentType;
import nerdhub.cardinal.components.api.component.extension.CopyableComponent;
import net.minecraft.nbt.CompoundTag;

public interface ReferenceCounter extends CopyableComponent<ReferenceCounter> {
    int push();
    int pop();

    class Impl implements ReferenceCounter {

        private int value = 0;

        @Override
        public int push() {
            if (value == Integer.MAX_VALUE)
                throw new RuntimeException("counter overflow");
            ++value;
            return value;
        }

        @Override
        public int pop() {
            if (value > 0)
                --value;
            return value;
        }

        @Override
        public void fromTag(CompoundTag compoundTag) {
            value = compoundTag.getInt("value");
        }

        @Override
        public CompoundTag toTag(CompoundTag compoundTag) {
            compoundTag.putInt("value", value);
            return compoundTag;
        }

        @Override
        public ComponentType<?> getComponentType() {
            return ChunkActivatorModInit.REFERENCE_COUNTER;
        }
    }
}
