package me.sargunvohra.mcmods.chunkactivator;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec2f;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.dimension.DimensionType;

import java.util.Objects;

public class ChunkActivatorBlockEntity extends BlockEntity {

    public ChunkActivatorBlockEntity() {
        super(ChunkActivatorModInit.CHUNK_ACTIVATOR_ENTITY);
    }

    private ReferenceCounter getReferenceCounter() {
        Objects.requireNonNull(world);
        Chunk chunk = world.getChunk(getPos());
        return ChunkActivatorModInit.REFERENCE_COUNTER.get(chunk);
    }

    private ServerCommandSource getCommandSource() {
        Objects.requireNonNull(world);
        return new ServerCommandSource(
            world.getServer(),
            new Vec3d(this.pos.getX() + 0.5D, this.pos.getY() + 0.5D, this.pos.getZ() + 0.5D),
            Vec2f.ZERO,
            (ServerWorld) world,
            4,
            "Chunk Activator",
            new LiteralText("Chunk Activator"),
            world.getServer(),
            null
        );
    }

    private void forceLoad(String addOrRemove) {
        World world = getWorld();
        if (world != null) {
            MinecraftServer server = world.getServer();
            if (server != null) {
                BlockPos pos = getPos();
                String command = String.format(
                    "forceload %s %s %s", addOrRemove, pos.getX(), pos.getZ());
                server.getCommandManager().execute(getCommandSource(), command);
            }
        }
    }

    @Override
    public void markRemoved() {
        super.markRemoved();
        if (world != null && !world.isClient) {
            if (getReferenceCounter().pop() == 0) {
                forceLoad("remove");
            }
        }
    }

    public void onPlaced() {
        if (world != null && !world.isClient) {
            if (getReferenceCounter().push() == 1) {
                forceLoad("add");
            }
        }
    }
}
